import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import Navbar from './components/Navbar';
import Details from './components/Details';
import Home from './components/Home';
import Search from './components/Search'
import { Route, Routes} from 'react-router-dom';

function App() {
  return (
    <div>
      <Navbar data-testid="navbar" />
      
      <Routes>
        <Route path="/details/:id" element={<Details data-testid="details" />} />
        <Route path="search/details/:id" element={<Details data-testid="details" />} />
        <Route path="/" element={<Home data-testid="home" />} />
        <Route path="/search" element={<Search data-testid="search" />} />       
      </Routes>
    </div>
  );
}

export default App;