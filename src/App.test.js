import { render } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import MyComponent from './App'; // Replace with the actual component being tested

test('renders the "home" navigation link', () => {
  const { getByText } = render(
    <Router>
      <MyComponent />
    </Router>
  );

  const homeNavLink = getByText('Home');
  expect(homeNavLink).toBeInTheDocument();
});

test('renders the "playlist" navigation link', () => {
  const { getByText } = render(
    <Router>
      <MyComponent />
    </Router>
  );

  const playlistNavLink = getByText('playlist');
  expect(playlistNavLink).toBeInTheDocument();
});

test('renders the "favorite" navigation link', () => {
  const { getByText } = render(
    <Router>
      <MyComponent />
    </Router>
  );

  const favoriteNavLink = getByText('favorite');
  expect(favoriteNavLink).toBeInTheDocument();
});

